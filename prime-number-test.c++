#include <iostream>


bool is_prime(int number)
{
	//check whether number is a prime
	for(int i = 2; i < number; ++i)
	{
		if(number % i  ==  0)
		{
			//found a divisor, thus number cannot be prime
			return false;
		}
	}
	
	//found no divisor, thus number must be prime
	return true;
}


int main(int argc, char* argv[])
{
	int number;
	
	//read number from standard input
	std::cout << "Enter an integer number: ";
	std::cin >> number;

	//print results
	if( is_prime(number) )
	{
		std::cout << number << " is a prime number." << std::endl;
	}
	else
	{
		std::cout << number << " is not a prime number." << std::endl;
	}
}
